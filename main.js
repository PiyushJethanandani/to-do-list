//Define UI Elements variables


const form = document.querySelector("#task-form");
const taskList = document.querySelector(".collection");
const clearBtn = document.querySelector(".clear-tasks");
const filter = document.querySelector("#filter");
const taskInput = document.querySelector("#task");


loadEventListener();


//Load All Events
function loadEventListener() {
    //Window Load Event
    document.addEventListener("DOMContentLoaded", getTasks);

    //Form Submit Event
    form.addEventListener("submit", addTask);

    //remove task Event
    taskList.addEventListener("click", removeTask);

    //clear task Event
    clearBtn.addEventListener("click", clearTasks);

    //Filter tasks event
    filter.addEventListener("keyup", filterTasks);
}


//Retrieve all tasks from LS


function getTasks() {
    let tasks;
    if (localStorage.getItem("tasks") === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem("tasks"));
    }

    tasks.forEach(function (task) {
        //create li element
        const li = document.createElement("li");

        //add class name
        li.className = "collection-items";

        //add text in li
        li.appendChild(document.createTextNode(task));
        //create a link for deletion
        const link = document.createElement("a");
        //add attribute
        link.setAttribute("href", "#");

        //add class
        link.className = "delete-item secondary-content";
        //add innerhtml as 'x' button to delete it
        link.innerHTML = "<i class='fa fa-remove'></li>";


        //append link to li
        li.appendChild(link);

        //append li to ul
        taskList.appendChild(li);
    });
}

//Add a new Task!

function addTask(e) {
    if (taskInput.value === '') {
        alert("Please do insert any task!");
    } else {
        //We have to create a new li and insert in ul


        //create li element
        const li = document.createElement("li");


        //add class name
        li.className = "collection-item";

        //add text in li
        li.appendChild(document.createTextNode(taskInput.value));

        //create a link for deletion
        const link = document.createElement("a");

        //add attribute
        link.setAttribute("href", "#");

        //add class
        link.className = "delete-item secondary-content";
        //add innerhtml as 'x' button to delete it
        link.innerHTML = "<i class='fa fa-remove'></li>";


        //append link to li
        li.appendChild(link);

        //append li to ul
        taskList.appendChild(li);

    }
    e.preventDefault();
}


function storeTaskInLocalStorage(task) {
    let tasks;
    if (localStorage.getItem("tasks") === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem("tasks"));
    }
    tasks.push(task);
    localStorage.setItem("tasks", JSON.stringify(tasks));
}

//REMOVE an Existing Task
function removeTask(e) {
    if ((e.target.parentElement.classList.contains('delete-item')) || e.target.classList.contains('delete-item')) {
        if (confirm("are you sure you want to delete?")) {
            let taskValue;
            if (e.target.parentElement.nodeName === 'Li') {
                taskValue = e.target.parentElement.textContent;
                e.target.parentElement.remove();
            } else {
                taskValue = e.target.parentElement.parentElement.textContent;
                e.target.parentElement.parentElement.remove();
            }
            removeTaskFromLocalStorage(taskValue);

        }
    }
}



function removeTaskFromLocalStorage(taskValue) {
    let tasks;
    if (localStorage.getItem("tasks") === null) {
        tasks = [];
    } else {
        tasks = JSON.parse(localStorage.getItem("tasks"));
    }
    tasks.forEach(function (task, index) {
        if (taskValue === task) {
            tasks.splice(index, 1);
        }
    });
    localStorage.setItem("tasks", JSON.stringify(tasks));
}


//Clear All Tasks
function clearTasks() {
    let tasks = [];
    //slower method
    //    taskList.innerHTML = '';

    //Faster method
    while (taskList.firstChild) {
        taskList.removeChild(taskList.firstChild);
    }

    localStorage.setItem("tasks", JSON.stringify(tasks));
}


//Filter Existing Task according to Key
function filterTasks(e) {
    const key = e.target.value.toLowerCase();

    document.querySelectorAll(".collection-item").forEach(function (task) {
        if (item.toLowerCase().indexOf(key) == -1) {
            task.style.display = 'none';
        } else {
            task.style.display = 'block';
        }
    });
}
